Required:
- write the README
- add some comments?

Details:
- 100 test coverage
- should I use more classes?
- use classes instead of plain constructors
- replace magic numbers with CONSTANTS
- specs for all the *public* methods
- clean the method for creating the acceptance tests 
- should I nest functions, to make them private? Or how do I make them private?
- how really is an injector output called
- in the case 8, the expected output is "Unable to comply". With an exception?
- in the optimum(), return the result as a hash

Done:
- change the repo name.
- order todo by importance
- replace starshipenterprise with WarpEngine?
- offer a command-line interface, to calculate for given values
- review all the names
- review the specs definitions
