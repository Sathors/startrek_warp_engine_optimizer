var Injector = require('./Injector')
var INJECTOR_NOMINAL_CAPACITY = new Injector().NOMINAL_CAPACITY

/*
 * The WarpEngine represents one of the models of the warp engine installed on
 * the Starship Enterprise.
 *
 * It has methods to calculate the necessary plasma flow to reach a certain
 * speed.
 */
function WarpEngine (damageToInjectorA, damageToInjectorB,
  damageToInjectorC, desiredSpeed) {
  this.REQUIRED_PLASMA_FLOW_FOR_LIGHTSPEED = 300
  this.desiredSpeed = desiredSpeed
  this.injectors = [damageToInjectorA, damageToInjectorB,
    damageToInjectorC].map((damage) => new Injector(damage))
}

WarpEngine.prototype.sustainableCapacity = function () {
  return this.injectors.reduce((a, b) => a + b.sustainableCapacity(), 0)
}

WarpEngine.prototype.overCapacityByInjector = function () {
  return this.overCapacity() / this.numberOfWorkingInjectors()
}

WarpEngine.prototype.operatingTime = function () {
  if (!this.isOverCapacity()) {
    return Infinity
  } else if (this.overCapacityByInjector() > INJECTOR_NOMINAL_CAPACITY) {
    return 0
  } else {
    return INJECTOR_NOMINAL_CAPACITY - this.overCapacityByInjector()
  }
}

WarpEngine.prototype.numberOfWorkingInjectors = function () {
  return this.injectors.filter((injector) => !injector.isDestroyed()).length
}

WarpEngine.prototype.totalDamage = function () {
  return this.injectors.reduce((a, b) => a + b.damage, 0)
}

WarpEngine.prototype.overCapacity = function () {
  return this.requiredCapacity() - this.sustainableCapacity()
}

WarpEngine.prototype.isOverCapacity = function () {
  return this.overCapacity() > 0
}

WarpEngine.prototype.maxCapacity = function () {
  return this.sustainableCapacity() +
    (INJECTOR_NOMINAL_CAPACITY - 1) * this.injectors.length
}

WarpEngine.prototype.optimumPlasmaFlow = function () {
  if (this.requiredCapacity() > this.maxCapacity()) {
    throw new Error('Unable to comply')
  } else if (this.sustainableCapacity() >= this.requiredCapacity()) {
    var requiredCapacityByInjector = this.requiredCapacity() /
      this.injectors.length
    return this.injectors.map(
      (injector) => Math.min(injector.sustainableCapacity(),
        requiredCapacityByInjector)
    )
  } else {
    return this.injectors.map(
      (injector) => injector.maxPlasmaFlowToOperateDuring(this.operatingTime())
    )
  }
}

WarpEngine.prototype.requiredCapacity = function () {
  return this.REQUIRED_PLASMA_FLOW_FOR_LIGHTSPEED * this.desiredSpeed / 100
}

module.exports = WarpEngine
