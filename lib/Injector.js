/*
 * The Injector represents one of the three injectors of a typical warp engine.
 * It injects the necessary plasma flow in the warp engine.
 */
function Injector (damage, plasmaFlow) {
  this.NOMINAL_CAPACITY = 100
  this.damage = damage
}

Injector.prototype.sustainableCapacity = function () {
  return this.NOMINAL_CAPACITY - this.damage
}

Injector.prototype.isDestroyed = function () {
  return this.damage === 100
}

Injector.prototype.maxPlasmaFlowToOperateDuring = function (operatingTime) {
  if (this.isDestroyed()) {
    return 0
  } else if (operatingTime === Infinity) {
    return this.sustainableCapacity()
  } else {
    var requiredOverCapacity = this.sustainableCapacity() - operatingTime
    return this.NOMINAL_CAPACITY + requiredOverCapacity
  }
}

module.exports = Injector
