describe('Injector', () => {
  var Injector = require('../lib/Injector')

  describe('sustainableCapacity', () => {
    it('returns the capacity at which the operating time is infinite', () => {
      expect(new Injector(0).sustainableCapacity()).toBe(100)
      expect(new Injector(30).sustainableCapacity()).toBe(70)
      expect(new Injector(32).sustainableCapacity()).toBe(68)
    })
  })

  describe('maxPlasmaFlowToOperateDuring', () => {
    describe('without damage', () => {
      var injector

      beforeEach(() => {
        injector = new Injector(0)
      })

      it('can go up to 100mg/s and operate indefinetely', () => {
        expect(injector.maxPlasmaFlowToOperateDuring(Infinity)).toBe(100)
      })

      it('has to overcharge when it has to operates for less than 100 minutes',
        () => {
          expect(injector.maxPlasmaFlowToOperateDuring(99)).toBe(101)
          expect(injector.maxPlasmaFlowToOperateDuring(1)).toBe(199)
          expect(injector.maxPlasmaFlowToOperateDuring(2)).toBe(198)
        })
    })

    describe('with damage', () => {
      it('can output 179mg/s to operate one minute when damaged 20%', () => {
        expect(new Injector(20).maxPlasmaFlowToOperateDuring(1)).toBe(179)
      })

      it('can output 178mg/s to operate two minutes when damaged 20%', () => {
        expect(new Injector(20).maxPlasmaFlowToOperateDuring(2)).toBe(178)
      })

      it('can output 80mg/s to operate indefinetely when damaged 20%', () => {
        expect(new Injector(20).maxPlasmaFlowToOperateDuring(Infinity)).toBe(80)
      })

      it('can output 81mg/s to operate 99 minutes when damaged 20%', () => {
        expect(new Injector(20).maxPlasmaFlowToOperateDuring(99)).toBe(81)
      })

      it('cannot have a plasma flow if it is destroyed', () => {
        expect(new Injector(100).maxPlasmaFlowToOperateDuring(99)).toBe(0)
        expect(new Injector(100).maxPlasmaFlowToOperateDuring(150)).toBe(0)
        expect(new Injector(100).maxPlasmaFlowToOperateDuring(5)).toBe(0)
      })
    })
  })
})
