function AcceptanceTest (damageA, damageB, damageC, speed, plasmaFlowA, plasmaFlowB, plasmaFlowC, operatingTime) {
  this.damageA = damageA
  this.damageB = damageB
  this.damageC = damageC
  this.speed = speed
  this.plasmaFlowA = plasmaFlowA
  this.plasmaFlowB = plasmaFlowB
  this.plasmaFlowC = plasmaFlowC
  this.operatingTime = operatingTime
}

describe('WarpEngine', () => {
  var WarpEngine = require('../lib/WarpEngine')

  describe('acceptance tests', () => {
    [
      new AcceptanceTest(0, 0, 0, 100, 100, 100, 100, Infinity),
      new AcceptanceTest(0, 0, 0, 90, 90, 90, 90, Infinity),
      new AcceptanceTest(0, 0, 0, 30, 30, 30, 30, Infinity),
      new AcceptanceTest(20, 10, 0, 100, 90, 100, 110, 90),
      new AcceptanceTest(0, 0, 100, 80, 120, 120, 0, 80),
      new AcceptanceTest(0, 0, 0, 150, 150, 150, 150, 50),
      new AcceptanceTest(0, 0, 30, 140, 150, 150, 120, 50)
    ].forEach((test, index) => {
      it('case ' + index, () => {
        var warpEngine = new WarpEngine(test.damageA,
          test.damageB, test.damageC, test.speed)
        expect(warpEngine.optimumPlasmaFlow()).toEqual(
          [test.plasmaFlowA, test.plasmaFlowB, test.plasmaFlowC])
        expect(warpEngine.operatingTime()).toBe(test.operatingTime)
      })
    })

    it('case 8', () => {
      var test = new AcceptanceTest(20, 50, 40, 170, 0, 0, 0, 0)
      var warpEngine = new WarpEngine(test.damageA, test.damageB, test.damageC,
        test.speed)
      expect(() => warpEngine.optimumPlasmaFlow()).toThrowError(Error,
        'Unable to comply')
      expect(warpEngine.operatingTime()).toBe(0)
    })
  })

  describe('requiredCapacity', () => {
    var warpEngine

    beforeEach(() => {
      warpEngine = new WarpEngine(0, 0, 0)
    })

    it('needs 300mg/s to go at 100% lightspeed', () => {
      warpEngine.desiredSpeed = 100
      expect(warpEngine.requiredCapacity()).toBe(300)
    })

    it('needs 150mg/s to go at 50% lightspeed', () => {
      warpEngine.desiredSpeed = 50
      expect(warpEngine.requiredCapacity()).toBe(150)
    })

    it('needs 450mg/s to go at 150% lightspeed', () => {
      warpEngine.desiredSpeed = 150
      expect(warpEngine.requiredCapacity()).toBe(450)
    })
  })

  describe('numberOfWorkingInjectors', () => {
    it('returns the number of non-destroyed injectors', () => {
      expect(new WarpEngine(0, 0, 0).numberOfWorkingInjectors()).toBe(3)
      expect(new WarpEngine(5, 5, 5).numberOfWorkingInjectors()).toBe(3)
      expect(new WarpEngine(100, 100, 100).numberOfWorkingInjectors()).toBe(0)
      expect(new WarpEngine(100, 0, 0).numberOfWorkingInjectors()).toBe(2)
    })
  })

  describe('totalDamage', () => {
    it('is the sum of the injectors damage', () => {
      expect(new WarpEngine(0, 0, 0).totalDamage()).toBe(0)
      expect(new WarpEngine(100, 100, 100).totalDamage()).toBe(300)
      expect(new WarpEngine(100, 100, 0).totalDamage()).toBe(200)
    })
  })
})
