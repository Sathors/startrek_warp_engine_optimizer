**Nombre**: Nicolas Schmitt  
**Email**: info@nicolaswebdev.com

# Instrucciones de uso

## Bajar el código
`$ git clone https://gitlab.com/Sathors/startrek_warp_engine_optimizer.git`

## Instalar los módulos node
Las dependencias javascript se instalan con un `$ yarn install`, en la raíz del proyecto.
En caso de que *yarn* no esté instalado: `$ sudo npm -g install yarn`.

## Correr las pruebas
`$ yarn test`

## Ver la cobertura de las pruebas
Correr las pruebas, y abrir el archivo `coverage/index.html`.

`$ firefox coverage/index.html`

## Correr el script
En la raíz:
`$ sudo npm -g install`

Después, el programa se llama con `$ optimize_warp_engine --help`.

Si no quiere instalarlo, puede usar `$ npm run optimize_warp_engine -- --help`.

# Observaciones sobre el proyecto

## Las técnologias y las herramientas
Es la primera vez que tuve que trabajar con Node.js, Yarn, Jasmine y ES6. Tenía algunas nociones de Javascript, aunque no había desarrollado nada en él.  
Por lo tanto, no estoy seguro que mi código siga las buenas practicas de Javascript (que cambian todos los seis meses). Pero sí creo que sigue buenas prácticas de programación en general.

Por mi falta de conocimiento en esas técnologías, me demore bastante, porque estaba investigando todos los temas que encontraba.

El enunciado decía que no se podían usar más librerías que Jasmine, pero sí use librerías de desarrollo, para ayudarme en el proceso:
- eslint, para el *linting* del código, siguiendo el [eslint-config-standard](https://github.com/feross/eslint-config-standard)
- istanbul, para generar la cobertura de las pruebas
- guard, una librería ruby que corre las pruebas cada vez que una archivo javascript se guarda. Eso sirve a reducir el *feedback loop*, lo que es muy importante para tener un TDD efectivo. Usé guard porque estaba acostumbrado a él, seguramente muchas librerías node permiten lo mismo.

También usé las siguientes herramientas:
- gitlab, para hostear el código
- vim, el editor que usé para escribir el código
- el terminal (bash), para todas las tareas necesarias

## El proceso de desarrollo
Desarrolle el programa usando TDD.

Esas fueron las etapas:
- crear las pruebas de aceptación, para saber cuando iba a estar cumpliendo con los requerimientos. Les deje en *skip*, para que no me den errores mientras estaba trabajando.
- empezar *bottom-up*, con los inyectores, creando pruebas a base de las especificaciones dadas.
- una vez que los inyectores funcionaban, empezar a desarrollar el StarshipEnterprise, que usa *composición* con los inyectores.
- en ese momento, me dí cuenta que unas de mis suposiciones iniciales en los inyectores no eran las mejores, así que tuve que *refactorizar* el código para poder seguir. Sigue la idea del TDD y del desarrollo ágil: hay que adaptarse.

Para dar un ejemplo concreto, mis inyectores daban el tiempo de operación en función de su sobrecarga, porque me parecía lo más lógico (y que el enunciado lo presentaba así). A la final, y según el *algoritmo* que encontré para calcular los valores para los tres inyectores, me dí cuenta que iba a calcular primero el tiempo de operación a nivel del motor, y a base de eso calcular la carga maximal para cada inyector. Así que tuve que cambiar mis inyectores para que me den la carga maximal en función del tiempo de operación.

## Los comentarios
No tengo la costumbre de usar muchos comentarios en mi código, porque sigo la recomendación de Martin Fowler: cuando encuentro una linea que podría necesitar un comentario, prefiero extraer ese pedazo en un método.

## El programa en linea de comandos
Es bien básico, porque el *parsing* de argumentos de la linea de comando es una tarea complicada, que en general se hace con librerías.

## Optimización
Prefiero privilegiar la facilidad de lectura del código, a optimizaciones prematuras. Por eso no uso *memoization* de las variables o de los resultados de las llamadas. Naturalmente, si hace falta optimizar para un requerimiento preciso, se puede hacer con *profiling* y refactorización.

## Validación de los inputs
Javascript es un lenguaje dínamico. No hay compilador que compruebe los argumentos de los métodos. Tampoco es costumbre de validar todos los argumentos manualmente, tomaría demasiado tiempo.  
Si el proyecto lo requiera (software critico), entonces se podría aumentar bastante su resistencia a inputs inesperados.

## Las pruebas
La cobertura es de 100%. Pero no todos los métodos tienen pruebas, muchos no lo justifican, por su sencillez. Esos métodos son métodos que serían *privados*, así que su funcionamiento ya está comprobado con las pruebas sobre los métodos *publicos*.
